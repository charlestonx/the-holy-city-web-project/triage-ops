# frozen_string_literal: true

require_relative '../group_definition'
require_relative '../constants/labels'
require_relative '../team_member_select_helper'

class ResourceGroup
  include TeamMemberSelectHelper

  UX_DEPARTMENT_REGEX = /ux department/i
  PRODUCT_DESIGNER_REGEX = /product designer/i
  QUALITY_DEPARTMENT_REGEX = /quality department/i
  SOFTWARE_ENGINEER_IN_TEST_REGEX = /software engineer in test/i

  attr_reader :group_name, :label_names

  def initialize(group_name, label_names)
    @group_name = group_name
    @label_names = label_names
  end

  def engineering_manager
    if label_names.include?(Labels::FRONTEND_LABEL)
      frontend_engineering_manager
    else
      backend_engineering_manager
    end
  end

  def frontend_engineering_manager
    GroupDefinition.em_for_team(group_name, :frontend)
  end

  def backend_engineering_manager
    GroupDefinition.em_for_team(group_name)
  end

  def product_manager
    GroupDefinition.pm_for_team(group_name)
  end

  def software_engineer_in_test
    parse_role_and_specialty_to_select_group_member(QUALITY_DEPARTMENT_REGEX, SOFTWARE_ENGINEER_IN_TEST_REGEX)
  end

  def product_designer
    parse_role_and_specialty_to_select_group_member(UX_DEPARTMENT_REGEX, PRODUCT_DESIGNER_REGEX)
  end

  private

  def parse_role_and_specialty_to_select_group_member(department_regex, role_regex)
    group_name_regex = Regexp.quote(group_name.tr('_', ' '))
    parsed_from_specialty =
      select_team_members_by_department_specialty_role(department_regex,
        /#{group_name_regex}/,
        role_regex,
        include_ooo: true).first

    # many team members don't populate the specialty field, but instead put their group info in the role field.
    parsed_from_role =
      select_team_members_by_department_specialty_role(department_regex,
        nil,
        /#{role_regex.source}.*#{group_name_regex}/,
        include_ooo: true).first

    parsed_from_specialty || parsed_from_role
  end
end
