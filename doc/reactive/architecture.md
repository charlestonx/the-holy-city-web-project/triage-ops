# Architecture

```mermaid
sequenceDiagram
    participant Webhook request
    %%participant ingress#8208nginx#8208controller
    %%participant triage#8208web#8208ingress (K8s Ingress)
    %%participant triage#8208web#8208service Rack app (K8s service)
    Webhook request->>Triage reactive service (config.ru): POST https://triage-ops.gitlab.com/ with JSON payload
    Triage reactive service (config.ru)->>WebhookEvent Rack middleware: Call middleware with JSON payload
    WebhookEvent Rack middleware->>Processor Rack app: Parse JSON payload and call app.call
    Processor Rack app->>Handler: Handler.new(event).process
    Handler->Processor list: //triage(event)
    loop For each processor
        Processor list-->Handler: Collect processors' results
    end
    Handler->>Processor Rack app: Returns `Hash<Processor name,Result object>`
    Processor Rack app->>WebhookEvent Rack middleware: Compute a Rack::Response with a JSON payload
    WebhookEvent Rack middleware->>Triage reactive service (config.ru): Pass along the reponse
    Triage reactive service (config.ru)->>Webhook request: Return an HTTP response
```
