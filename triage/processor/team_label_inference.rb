# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/www_gitlab_com'

module Triage
  class TeamLabelInference < Processor
    react_to 'issue.open', 'issue.reopen', 'issue.update',
      'merge_request.open', 'merge_request.reopen', 'merge_request.update'

    # these labels exist in the sytem but are not supported by our groups and stages api
    UNSUPPORTED_DEVOPS_LABELS = [].freeze
    UNSUPPORTED_GROUP_LABELS = [
      'group::ai-integration',
      'group::incubation',
      'group::pricing',
      'group::ux paper cuts'
    ].freeze

    # Applicable when a section::, devops:: or group:: label is present/changed
    # @return [Boolean] true when
    def applicable?
      event.from_gitlab_org? &&
        !event.gitlab_bot_event_actor? &&
        event.label_names.any? { |l| l.match?(/section::.+|devops::.+|group::.+/) }
    end

    def process
      @new_labels = {}

      validate_labels

      add_comment(comment, append_source_link: false) if @new_labels.any?
    end

    def documentation
      <<~TEXT
        Section, Stage and Group labels must align.

        Group will always take priority over Stage and Section labels. This will allow for the least amount of
        changes. I.e., when a Group label is added/updated, Stage and Section labels are updated.

        - When no Group Stage or Section labels exist: Do nothing
        - When only a Section label is present: Do nothing
        - When only a Group label is present: Infer Stage and Section label based on the Group
        - When only a Stage label is present: Infer Section label based on the Stage
        - When only Stage and Group labels are present
          - If the Stage and Group labels match: Only infer the Section label
          - If the Stage and Group labels mismatch: Change the Stage and Section labels based on the Group
        - When only Section and Group labels are present
          - If the Section and Group labels match: Only infer the Stage label
          - If the Section and Group labels mismatch: Change the Section and Stage labels based on the Group
        - When all Section, Stage and Group labels are present
          - If the Section does not match the Group: Change the Section label based on the Group
          - If the Stage does not match the Group: Change the Stage label based on the Group

        Examples:
          Good:
            "section::dev" "devops::create" "group::source code"
            "section::dev" "devops::plan" "group::ide"

          Bad:
            "section::ops" "devops::create" "group::source code" #=> Problem: Incorrect Section; Action: change section::ops to section::dev
            "devops::create" "group::source code" #=> Problem: No Section; Action: add section::dev
            "section::ops" #=> Problem: Missing Stage and Group labels; Action: Leave a note for adding Stage and Group labels
            "section::dev" "devops::create" #=> Problem: Missing Group label; Action: Do nothing.
            "devops::plan" "group::ide" => Problem: No Section, Incorrect Stage; Action: add section::dev, change devops::plan to devops::create

        Note: This processor depends on a valid Enterprise License to support Scoped Labels.  If no EE
              license is present, you may receive two identical comments during an update.
      TEXT
    end

    private

    def label_names
      @label_names ||= event.label_names
    end

    def stages_json_definition
      @stages_json_definition ||= WwwGitLabCom.stages
    end

    def groups_json_definition
      @groups_json_definition ||= WwwGitLabCom.groups
    end

    def validate_labels
      validate_group_label if group_label
      validate_devops_label if devops_label
      validate_section_label if section_label
    end

    # Validates the ~group:: label
    def validate_group_label
      infer_section_from_group unless section_label
      infer_devops_from_group unless devops_label

      # validate that the existing devops label applies to the group. If not, change it
      infer_devops_from_group if expected_devops_label && devops_label != expected_devops_label
    end

    # Validates the ~devops:: label
    def validate_devops_label
      infer_section_from_devops if expected_section_label && section_label != expected_section_label
    end

    # Validates the ~section:: label
    def validate_section_label
      return unless validated_stage_name_from_devops_label && validated_group_name

      infer_section_from_group if expected_section_label && section_label != expected_section_label
    end

    # Given a Group label, infer the ~devops:: label
    def infer_devops_from_group
      @new_labels[expected_devops_label] = group_label
    end

    # Given a Group label, infer the ~section:: label
    def infer_section_from_group
      @new_labels[expected_section_label] = group_label
    end

    def infer_section_from_devops
      @new_labels[expected_section_label] = devops_label
    end

    # @return [String,nil] the full name of the group label, excluding unsupported labels
    # @see #validated_group_name to return the label name (without the scope)
    def group_label
      label_names.find do |label_name|
        label_name.start_with?('group::') && !UNSUPPORTED_GROUP_LABELS.include?(label_name)
      end
    end

    # @return [String,nil] the full name of the devops label, excluding unsupported labels
    # @see #validated_stage_name_from_devops_label to return the label name (without the scope)
    def devops_label
      label_names.find do |label_name|
        label_name.start_with?('devops::') && !UNSUPPORTED_DEVOPS_LABELS.include?(label_name)
      end
    end

    # @return [String,nil] the full name of the section label
    def section_label
      label_names.find { |l| l.start_with?('section::') }
    end

    def unscope_label(label, split: '::')
      label.split(split).last&.tr(' ', '_')
    end

    def label_to_group(label)
      label.downcase.gsub(/[:| ]+/, '_')
    end

    def validated_group_name
      return unless group_label

      parsed_group_name = parse_group_name_from_label(group_label)

      raise ArgumentError, "'#{parsed_group_name}' group is not found in groups JSON definition." if groups_json_definition[parsed_group_name].nil?

      parsed_group_name
    end

    def parse_group_name_from_label(group_label)
      derived_group_label =
        case group_label
        when 'group::distribution'
          # Labels such as group::distribution::build and group::distribution::deploy can be used to identify the specific group
          label_names.find { |label| label.start_with?('group::distribution::') } || 'distribution_build'
        when 'group::gitaly'
          # Labels such as group::gitaly::cluster and group::gitaly::git can be used to identify the specific group
          label_names.find { |label| label.start_with?('group::gitaly::') } || 'gitaly_cluster'
        else
          group_label
        end

      label_to_group(unscope_label(derived_group_label, split: 'group::'))
    end

    def validated_stage_name_from_devops_label
      return unless devops_label

      parsed_stage_name = unscope_label(devops_label)

      raise ArgumentError, "'#{parsed_stage_name}' stage is not found in stages JSON definition." if stages_json_definition[parsed_stage_name].nil?

      parsed_stage_name
    end

    # Based on the ~group:: label, determine what the appropriate ~devops:: label should be
    def expected_devops_label
      return unless validated_group_name

      expected_stage_from_validated_group = groups_json_definition[validated_group_name]['stage']

      stages_json_definition[expected_stage_from_validated_group]['label']
    end

    # Based on the ~devops:: label, determine what the appropriate ~section:: label should be
    # @note Group takes precedence over the Stage label
    def expected_section_label
      return "section::#{groups_json_definition[validated_group_name]['section']}" if validated_group_name

      "section::#{stages_json_definition[validated_stage_name_from_devops_label]['section']}" if validated_stage_name_from_devops_label
    end

    # Return a GitLab-formatted label
    # @return [String] GitLab-formatted label markdown
    # @example
    #   markdown_label('test') #=> %(~"test")
    #   markdown_label('scoped::label') #=> %(~"scoped::label")
    def markdown_label(label)
      %(~"#{label}")
    end

    # Markdown comment to be left on the Issuable detailing what changes were made
    # and why
    # @return [String] GitLab-friendly Markdown
    def comment
      quick_action_labels = @new_labels.keys.map { |label| markdown_label(label) }

      <<~MARKDOWN.chomp
        /label #{quick_action_labels.join(' ')}
      MARKDOWN
    end
  end
end
