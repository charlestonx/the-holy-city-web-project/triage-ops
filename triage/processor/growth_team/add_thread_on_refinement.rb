# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../../lib/constants/labels'

module Triage
  class AddThreadOnRefinement < Processor
    REFINE_LABEL = Labels::WORKFLOW_REFINEMENT_LABEL
    GROWTH_LABELS = Labels::GROWTH_TEAM_LABELS

    react_to 'issue.update', 'issue.open'

    def applicable?
      event.from_gitlab_org? &&
        has_growth_labels? &&
        refine_label_added?
    end

    def process
      add_refinement_thread
    end

    def documentation
      <<~TEXT
      This processor add the growth team refinement thread to an issue that
      belongs to the team when it is moved to `workflow::refinement`
      TEXT
    end

    private

    def has_growth_labels?
      GROWTH_LABELS.all? { |label| event.added_label_names.include?(label) || event.label_names.include?(label) }
    end

    def refine_label_added?
      event.added_label_names.include?(REFINE_LABEL)
    end

    def add_refinement_thread
      comment = <<~MARKDOWN.chomp
      ## :construction: Refinement
      Welcome to the refinement stage for this issue. During this stage, engineers will ensure that this issue meets our criteria for issues ready for development.

      Please follow these guidelines to ensure a thorough refinement:
      - Verify the issue description and check that the requirements are present and well-defined
      - Evaluate risks and dependencies
      - Uncover blind spots if possible
      - Outline possible technical solutions for the problem

      :warning: Make sure every outcome of the refinement discussion is included in the issue's description. We want to avoid needing to go over multiple comments and threads to figure out what is the decision or additional requirement. The description should be the single source of truth when it comes to implementation details.

      As an engineer, please:
      - Discuss and update the issue description (if needed)
      - Apply a weight estimate by voting :one: :two: :three: :five: or :rocket: (if above 5) - :rocket: means it's likely too big and needs splitting into smaller issues (also please suggest how by starting a discussion)
      - Ensure the issue has a `~type` label

      This issue will be considered as completed with refinement if it receives 3 unique weight estimations, and moved to ~"workflow::scheduling" afterwards.

      /cc @gitlab-org/growth/engineers
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
