# frozen_string_literal: true

require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class FailureTrace
      FAILURE_TRACE_MARKERS = {
        rspec: { start: "Failures:\n", end: "\n[TEST PROF INFO]", language: 'ruby', label: '~backend' },
        jest: { start: "Summary of all failing tests\n", end: "\nRan all test suites.", language: 'javascript', label: '~frontend' }
      }.freeze

      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human

      attr_reader :trace, :config, :framework, :failed_examples_data

      def initialize(trace:, config:, failed_examples_data:)
        @trace = trace
        @config = config
        @framework = identify_framework
        @failed_examples_data = failed_examples_data || []
      end

      def summary_markdown
        return unless framework

        <<~MARKDOWN.chomp
        ```#{markers[:language]}
        #{failure_summary[...FAILURE_TRACE_SIZE_LIMIT]}
        ```

        /label #{markers[:label]}
        MARKDOWN
      end

      def attribution_message_markdown
        message = failed_examples_data.map do |example|
          "- ~\"#{example['product_group_label']}\" ~\"#{example['feature_category_label']}\" #{example['id']}"
        end.join("\n")

        <<~MARKDOWN.chomp
          #{message}
        MARKDOWN
      end

      def root_cause_label
        label_key = config.root_cause_to_trace_map.detect do |_root_cause, trace_patterns|
          trace_patterns.any? { |pattern| trace.downcase.include?(pattern.downcase) }
        end&.first || :default

        Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.fetch(label_key)
      end

      def top_group_label
        potential_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def potential_group_labels
        @potential_group_labels ||= failed_examples_data.filter_map { |example| example['product_group_label'] }
      end

      private

      def identify_framework
        FAILURE_TRACE_MARKERS.each do |framework, markers|
          return framework if trace.include?(markers[:start]) && trace.include?(markers[:end])
        end

        nil
      end

      def failure_summary
        trace.split(markers[:start]).last.split(markers[:end]).first.chomp
      end

      def markers
        FAILURE_TRACE_MARKERS.fetch(framework)
      end
    end
  end
end
