# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class ReviewAppChildPipeline < Base
        INCIDENT_PROJECT_ID = '43330483' # gitlab-org/quality/engineering-productivity/review-apps-broken-incidents
        INCIDENT_LABELS = ['review-apps-broken', 'Engineering Productivity'].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN
          ## %<project_link>s pipeline %<pipeline_link>s failed

          **Branch: %<branch_link>s**

          **Commit: %<commit_link>s**

          **Triggered by** %<triggered_by_link>s • **Source:** %<source>s • **Duration:** %<pipeline_duration>s minutes

          **Failed jobs (%<failed_jobs_count>s):**

          %<failed_jobs_list>s

          ### General guidelines

          Please refer to [the review-apps triaging process](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/review-apps.md#review-apps-broken-slack-channel-isnt-empty).
        MARKDOWN

        SLACK_CHANNEL = 'review-apps-broken'

        def self.match?(event)
          event.on_instance?(:com) &&
            event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            event.source_job_id &&
            event.variables.any? {  |h| h['key'] == 'REVIEW_APPS_DOMAIN' } && # child pipeline
            event.builds.any? { |job| job['name'] == 'review-deploy' && job['status'] == 'failed' }
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          INCIDENT_LABELS
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end
      end
    end
  end
end
