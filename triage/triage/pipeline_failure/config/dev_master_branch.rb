# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class DevMasterBranch < Base
        SLACK_CHANNEL = 'master-broken-mirrors'

        def self.match?(event)
          event.on_instance?(:dev) &&
            event.project_path_with_namespace == 'gitlab/gitlab-ee' &&
            !event.merge_request_pipeline? &&
            event.ref == 'master' &&
            event.source_job_id.nil?
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end
      end
    end
  end
end
