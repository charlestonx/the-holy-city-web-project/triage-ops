# frozen_string_literal: true

require_relative 'concerns/periscope_csv'

module Triage
  class OrgByUsernameLocator
    include PeriscopeCsv

    CSV_URL_VAR = 'USER_ACCOUNT_CSV_URL'
    USER_ID_HEADER_NAME = 'USER_ID'
    ACCOUNT_ID_HEADER_NAME = 'CRM_ACCOUNT_ID'
    CSV_CACHE_EXPIRY = 14400

    def locate_org(id)
      matching_row = csv_map.find { |row| row[USER_ID_HEADER_NAME] == id.to_s }
      matching_row&.fetch(ACCOUNT_ID_HEADER_NAME)
    end
  end
end
