# frozen_string_literal: true

require_relative '../triage'

module Triage
  module Reaction
    module_function

    def add_comment(body, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/notes"

      Reaction.post_request(path, body, append_source_link: append_source_link)
    end
    public :add_comment

    def add_discussion(body, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/discussions"

      Reaction.post_request(path, body, append_source_link: append_source_link)
    end
    public :add_discussion

    def append_discussion(body, discussion_id, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes"

      Reaction.post_request(path, body, append_source_link: append_source_link)
    end
    public :append_discussion

    def merge_merge_request(noteable_path = event.noteable_path)
      path = "#{noteable_path}/merge"

      Triage.api_client.put(path, body: { merge_when_pipeline_succeeds: true }) unless Triage.dry_run?

      "PUT #{PRODUCTION_API_ENDPOINT}#{path}"
    end
    public :merge_merge_request

    def update_discussion_comment(body, discussion_id, note_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes/#{note_id}"

      Reaction.put_request(path, body: body)
    end
    public :update_discussion_comment

    def resolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, resolved: true)
    end
    public :resolve_discussion

    def unresolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, resolved: false)
    end
    public :unresolve_discussion

    def self.post_request(path, body, append_source_link: false)
      body = add_automation_suffix(body) if append_source_link

      Triage.api_client.post(path, body: { body: body }) unless Triage.dry_run?

      "POST #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end

    def self.put_request(path, body)
      Triage.api_client.put(path, body: body) unless Triage.dry_run?

      "PUT #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end

    def self.add_automation_suffix(body)
      source_path = get_source_path(caller_locations(1..5))
      return body unless source_path

      <<~MARKDOWN.chomp
        #{body}

        *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/#{source_path}).*
      MARKDOWN
    end

    def self.get_source_path(locations)
      processor_location = locations.find { |location| location.path.match?(%r{/triage/(processor|job)/}) }
      return unless processor_location

      processor_location.path.partition('/triage/').last
    end
  end
end
