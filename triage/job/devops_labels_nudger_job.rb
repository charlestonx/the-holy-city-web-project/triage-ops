# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/unique_comment'
require_relative '../triage/devops_labels_validator'
require_relative '../resources/issue'

module Triage
  class DevopsLabelsNudgerJob < Job
    include Reaction

    LABELS_MISSING_MESSAGE = <<~MARKDOWN.strip
      Please add a [group](https://docs.gitlab.com/ee/development/labels/#group-labels) or
      [category](https://docs.gitlab.com/ee/development/labels/#category-labels) label to identify issue ownership.

      You can refer to the [Features by Group](https://about.gitlab.com/handbook/product/categories/features/#features-by-group) handbook page for guidance.

      If you are unsure about the correct group, please do not leave the issue without a group label, and refer to
      [GitLab's shared responsibility functionality guidelines](https://about.gitlab.com/handbook/product/categories/#shared-responsibility-functionality)
      for more information on how to triage this kind of issue.
    MARKDOWN

    private

    def execute(event)
      prepare_executing_with(event)

      add_devops_labels_reminder if applicable?
    end

    def applicable?
      event.from_gitlab_org_gitlab? &&
        event.by_team_member? &&
        !labels_set? &&
        !previous_discussion?
    end

    def labels_set?
      DevopsLabelsValidator.new(issue.labels).labels_set?
    end

    def issue
      @issue ||= Triage::Issue.new(Triage.api_client.issue(event.project_id, event.iid).to_h)
    end

    def add_devops_labels_reminder
      comment = reminder_text
      add_comment(comment, append_source_link: true)
    end

    def reminder_text
      user_name = event.event_actor_username

      message_markdown = <<~MARKDOWN.chomp
        :wave: @#{user_name}, #{LABELS_MISSING_MESSAGE}
      MARKDOWN

      unique_comment.wrap(message_markdown).strip
    end

    def previous_discussion?
      unique_comment.previous_discussion
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new("Triage::DevopsLabelsNudgerJob--DevopsLabelsMissing", event)
    end
  end
end
