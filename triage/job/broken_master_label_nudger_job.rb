# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/unique_comment'
require_relative '../triage/broken_master_incident_label_validator'

module Triage
  class BrokenMasterLabelNudgerJob < Job
    include Reaction

    ROOT_CAUSE_LABEL_MISSING_MESSAGE = 'please add a concrete ~master-broken:: root cause label'
    FLAKY_REASON_LABEL_MISSING_MESSAGE = 'please identify the flaky root cause with a ~flaky-test:: label.'

    private

    def execute(event)
      prepare_executing_with(event)

      add_root_cause_label_reminder if validator.need_root_cause_label?
      add_flaky_reason_label_reminder if validator.need_flaky_reason_label?
    end

    def add_root_cause_label_reminder
      comment = reminder_text('RootCauseLabelMissing', ROOT_CAUSE_LABEL_MISSING_MESSAGE)
      add_comment(comment, append_source_link: true)
    end

    def add_flaky_reason_label_reminder
      comment = reminder_text('FlakyReasonLabelMissing', FLAKY_REASON_LABEL_MISSING_MESSAGE)
      add_comment(comment, append_source_link: true)
    end

    def validator
      @validator ||= BrokenMasterIncidentLabelValidator.new(event)
    end

    def reminder_text(unique_comment_identifier, message)
      user_name = event.event_actor_username

      message_markdown = <<~MARKDOWN.chomp
        :wave: @#{user_name}, #{message}
      MARKDOWN

      unique_comment(unique_comment_identifier).wrap(message_markdown).strip
    end

    def unique_comment(unique_comment_identifier)
      @unique_comment ||= UniqueComment.new("Triage::BrokenMasterLabelNudgerJob--#{unique_comment_identifier}", event)
    end
  end
end
