# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/keep_cache_warm_job'

RSpec.describe Triage::KeepCacheWarmJob do
  describe '#perform' do
    before do
      allow(subject).to receive(:refresh)
    end

    it 'refreshes cache we are using' do
      allow(subject).to receive(:refresh).and_call_original

      expect(Triage).to receive(:gitlab_org_group_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_com_group_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_org_group_member_usernames).with(fresh: true)
      expect(Triage).to receive(:gitlab_com_group_member_usernames).with(fresh: true)
      expect(Triage).to receive(:gitlab_com_appsec_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_org_contractors_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_com_support_member_ids).with(fresh: true)
      expect(Triage).to receive(:gitlab_docs_team_member_ids).with(fresh: true)
      expect(Triage).to receive(:jihu_team_member_ids).with(fresh: true)

      subject.perform
    end

    it 'schedules next same job' do
      expect(described_class).to receive(:perform_in).with(Triage::GROUP_CACHE_DEFAULT_EXPIRATION - 60)

      subject.perform
    end

    context 'when refreshing raising an error' do
      before do
        allow(subject).to receive(:refresh).and_raise(RuntimeError)
      end

      it 'schedules next same job' do
        expect(described_class).to receive(:perform_in).with(Triage::GROUP_CACHE_DEFAULT_EXPIRATION - 60)

        expect { subject.perform }.to raise_error(RuntimeError)
      end
    end
  end

  describe '#perform_async' do
    before do
      stub_api_request(
        path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members",
        query: { per_page: 100 })
    end

    it 'schedules next same job' do
      expect(described_class).to receive(:perform_in).with(Triage::GROUP_CACHE_DEFAULT_EXPIRATION - 60)

      described_class.perform_async

      described_class.drain!
    end

    context 'when refreshing raising an error' do
      before do
        stub_api_request(
          path: "/groups/#{Triage::GITLAB_ORG_GROUP}/members",
          query: { per_page: 100 },
          response_status: 500)
      end

      it 'schedules next same job and capture errors' do
        logger = SuckerPunch.logger

        expect(described_class).to receive(:perform_in).with(Triage::GROUP_CACHE_DEFAULT_EXPIRATION - 60)
        expect(Raven).to receive(:tags_context).with(job: described_class, process: :sucker_punch, service: 'reactive').ordered
        expect(Raven).to receive(:extra_context).with(job_args: []).ordered
        expect(Raven).to receive(:capture_exception).with(kind_of(Gitlab::Error::InternalServerError)).ordered
        expect(logger).to receive(:info)
        expect(logger).to receive(:error).with(kind_of(Gitlab::Error::InternalServerError), klass: described_class, args: [])

        described_class.perform_async

        described_class.drain!
      end
    end
  end
end
