# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/failed_jobs'

RSpec.describe Triage::PipelineFailure::FailedJobs do
  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_id: 999)
  end

  subject { described_class.new(event) }

  describe '#execute' do
    let(:first_job) { Gitlab::ObjectifiedHash.new('name' => 'first', 'web_url' => 'first_url', 'allow_failure' => false) }
    let(:second_job) { Gitlab::ObjectifiedHash.new('name' => 'second', 'web_url' => 'second_url', 'allow_failure' => false) }
    let(:third_job_downstream_url) { 'third_downstream_url' }
    let(:third_job_downstream_pipeline) { Gitlab::ObjectifiedHash.new('web_url' => third_job_downstream_url) }
    let(:third_job) { Gitlab::ObjectifiedHash.new('name' => 'third', 'web_url' => 'third_url', 'allow_failure' => false, 'downstream_pipeline' => third_job_downstream_pipeline) }
    let(:api_client_double) { double('API client') }
    let(:pipeline_jobs) { double('pipeline_jobs') }
    let(:pipeline_bridges) { double('pipeline_bridges') }

    before do
      allow(Triage).to receive(:api_client).and_return(api_client_double)
      allow(Triage.api_client).to receive(:pipeline_jobs)
        .and_return(pipeline_jobs)
      allow(pipeline_jobs).to receive(:auto_paginate)
        .and_yield(first_job)
        .and_yield(second_job)
      allow(Triage.api_client).to receive(:pipeline_bridges)
        .and_return(pipeline_bridges)
      allow(pipeline_bridges).to receive(:auto_paginate)
        .and_yield(Gitlab::ObjectifiedHash.new('name' => 'bar', 'allow_failure' => true))
        .and_yield(third_job)
    end

    it 'fetches the failed jobs and return the ones that are not allowed to fail' do
      expect(Triage.api_client).to receive(:pipeline_jobs)
        .with(event.project_id, event.id, scope: 'failed', per_page: 100)
        .and_return(pipeline_jobs)
      expect(Triage.api_client).to receive(:pipeline_bridges)
        .with(event.project_id, event.id, scope: 'failed', per_page: 100)
        .and_return(pipeline_bridges)

      expect(subject.execute.map(&:name)).to eq([first_job.name, second_job.name, third_job.name])
    end

    it 'replaces the bridge job URL with the actual downstream pipeline URL' do
      expect(subject.execute.map(&:web_url)).to eq([first_job.web_url, second_job.web_url, third_job_downstream_url])
    end

    context 'when downstream pipeline is nil' do
      let(:third_job_downstream_pipeline) { nil }

      it 'skips the bridge job URL' do
        expect(subject.execute.map(&:web_url)).to eq([first_job.web_url, second_job.web_url])
      end
    end
  end
end
