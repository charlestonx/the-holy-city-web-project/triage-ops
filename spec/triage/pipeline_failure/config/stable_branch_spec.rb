# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/stable_branch'

RSpec.describe Triage::PipelineFailure::Config::StableBranch do
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:ref) { '15-10-stable-ee' }
  let(:event_actor) { Triage::User.new(id: 42) }
  let(:merge_request) { Triage::MergeRequest.new(author: { 'id' => 42 }) }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      ref: ref,
      event_actor: event_actor,
      merge_request: merge_request)
  end

  before do
    allow(event).to receive(:on_instance?).with(:com).and_return(true)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when event is not on the .com instance' do
      before do
        allow(event).to receive(:on_instance?).with(:com).and_return(false)
      end

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when project_path_with_namespace is not "gitlab-org/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when ref is not /^[\d-]+-stable(-ee)?$/' do
      let(:ref) { 'foo' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#incident_project_id' do
    it 'returns expected projet id' do
      expect(subject.incident_project_id).to eq('5064907')
    end
  end

  describe '#incident_template' do
    it 'returns expected template' do
      expect(subject.incident_template).to eq(described_class::INCIDENT_TEMPLATE)
    end
  end

  describe '#incident_labels' do
    it 'returns expected labels' do
      expect(subject.incident_labels).to eq(['release-blocker'])
    end
  end

  describe '#incident_extra_attrs' do
    it 'returns expected attrs' do
      expect(subject.incident_extra_attrs).to eq({ assignee_ids: [event_actor.id, merge_request.author['id']] })
    end
  end

  describe '#default_slack_channels' do
    it 'returns expected channels' do
      expect(subject.default_slack_channels).to eq(['releases'])
    end
  end
end
