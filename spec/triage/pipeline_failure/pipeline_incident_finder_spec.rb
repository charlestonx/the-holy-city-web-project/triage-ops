# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage'
require_relative '../../../triage/triage/pipeline_failure/pipeline_incident_finder'

RSpec.describe Triage::PipelineFailure::PipelineIncidentFinder do
  let(:api_client_double) { double('API Client') }
  let(:project_id)        { 1 }
  let(:pipeline_id)       { 2 }
  let(:incident_double)   { double('Incident') }
  let(:incidents)         { [incident_double] }

  subject { described_class.new(incident_project_id: project_id, pipeline_id: pipeline_id) }

  before do
    allow(Triage).to receive(:api_client).and_return(api_client_double)
  end

  describe '#latest_incident' do
    before do
      allow(api_client_double).to receive(:issues).and_return(incidents)
    end

    it 'returns the last incident' do
      expect(subject.latest_incident).to eq(incident_double)
    end
  end

  describe '#incidents' do
    let(:error_response_double) do
      double('Response',
        code: 500,
        parsed_response: '500 Internal Server Error',
        request: double('Request Double', base_uri: '', path: 'projects/1/issues'))
    end

    context 'when issues api returns an array of incidents' do
      before do
        allow(api_client_double).to receive(:issues).and_return(incidents)
      end

      it 'returns the array of incidents' do
        expect(subject.incidents).to eq(incidents)
      end
    end

    context 'when issues api throws Gitlab::Error::InternalServerError 2 times before returns the incidents' do
      before do
        call_count = 0
        allow(api_client_double).to receive(:issues) do
          call_count += 1
          call_count <= 2 ? raise(Gitlab::Error::InternalServerError, error_response_double) : incidents
        end
      end

      it 'calls the issues api a total of 3 times and returns the final incidents' do
        expect(api_client_double).to receive(:issues).exactly(3).times
        expect(subject.incidents).to eq(incidents)
      end
    end

    context 'when issues api throws Gitlab::Error::InternalServerError 3 times' do
      before do
        allow(api_client_double).to receive(:issues).and_raise(Gitlab::Error::InternalServerError, error_response_double)
      end

      it 'calls the issues api a total of 3 times and returns []' do
        expect(api_client_double).to receive(:issues).exactly(3).times
        expect(subject.incidents).to eq([])
      end
    end
  end
end
