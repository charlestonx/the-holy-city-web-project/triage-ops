# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/require_type_on_refinement'
require_relative '../../triage/triage/event'

RSpec.describe Triage::RequireTypeOnRefinement do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:event_attrs) do
      {
        from_part_of_product_project?: true,
        type_label_set?: false,
        added_label_names: added_label_names
      }
    end

    let(:added_label_names) { ['workflow::ready for development'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not a part of product' do
      before do
        allow(event).to receive(:from_part_of_product_project?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when ~"workflow::ready for development" is not added' do
      before do
        allow(event).to receive(:added_label_names).and_return([])
      end

      include_examples 'event is not applicable'
    end

    context 'when type label is set' do
      before do
        allow(event).to receive(:type_label_set?).and_return(true)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = add_automation_suffix('processor/require_type_on_refinement.rb') do
        <<~MARKDOWN.chomp
          @root, thank you for moving this issue to ~"workflow::ready for development".

          Please add a `type::` label to indicate the
          [work type classification](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification).
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
