# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_help'

RSpec.describe Triage::CommandMrHelp do
  include_context 'with discord posting context', 'DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HELP'
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        new_comment: %(@gitlab-bot help),
        url: url,
        title: title,
        resource_author: resource_author,
        project_path_with_namespace: project_path_with_namespace,
        label_names: label_names
      }
    end

    let(:url) { 'http://gitlab.com/mr_url' }
    let(:title) { 'MR Title' }
    let(:resource_author) { Triage::User.new(username: 'mr_author_username') }
    let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
    let(:label_names) { ['backend', 'frontend', 'documentation', 'test::label'] }
  end

  let(:coach_username) { '@coach_username' }
  let(:by_team_member) { false }
  let(:targets_public_project) { true }

  subject { described_class.new(event) }

  before do
    allow(subject).to receive(:select_random_merge_request_coach).and_return(coach_username)
    allow(event).to receive(:by_team_member?).and_return(by_team_member)
    allow(event).to receive(:project_public?).and_return(targets_public_project)
    allow(discord_messenger_stub).to receive(:send_message)
  end

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', 'help'

  describe '#applicable?' do
    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution command processor #applicable?'

    context 'when actor is not a team member' do
      it_behaves_like 'rate limited', count: 1, period: 3600
    end

    context 'when actor is a team member' do
      let(:by_team_member) { true }

      it_behaves_like 'rate limited', count: 100, period: 3600
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment to reply to author and pings MR coaches' do
      body = add_automation_suffix('processor/community/command_mr_help.rb') do
        <<~MARKDOWN.chomp.concat("\n")
          Hey there #{coach_username}, could you please help @root out?
          /assign_reviewer #{coach_username}
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end

    context 'when posting messages to discord' do
      before do
        allow(subject).to receive(:add_comment)
      end

      context 'when merge request has labels' do
        context 'with no reviewers given' do
          it_behaves_like 'discord message posting' do
            let(:message_body) do
              format_discord_message('`backend`, `frontend`, `documentation`, `test::label`')
            end
          end
        end
      end

      context 'when merge request has no labels' do
        let(:label_names) { [] }

        context 'with no reviewers given' do
          it_behaves_like 'discord message posting' do
            let(:message_body) { format_discord_message('none') }
          end
        end
      end

      context 'when merge request targets a non public project' do
        let(:targets_public_project) { false }

        it_behaves_like 'no discord message posting'
      end
    end
  end

  it_behaves_like 'instantiates discord messenger'

  def format_discord_message(labels)
    <<~TEXT
      `#{event.resource_author.username}` is looking for help on a merge request:
      - URL: #{event.url}
      - Title: `#{event.title}`
      - Project: `#{event.project_path_with_namespace}`
      - Labels: #{labels}
    TEXT
  end
end
