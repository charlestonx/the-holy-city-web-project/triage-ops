# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/remove_idle_labels_on_activity'

RSpec.describe Triage::RemoveIdleLabelOnActivity do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        by_resource_author?: by_resource_author,
        by_team_member?: by_team_member
      }
    end

    let(:by_resource_author) { true }
    let(:by_team_member) { false }
    let(:label_names) { [Labels::IDLE_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.note']

  describe '#applicable?' do
    it_behaves_like 'community contribution processor #applicable?'

    context 'when event is for a community contribution' do
      include_context 'with community contribution processor'

      context 'when the comment is not from the resource author nor a team member' do
        let(:by_resource_author) { false }

        include_examples 'event is not applicable'
      end

      context 'when the comment is from a team member' do
        let(:by_resource_author) { false }
        let(:by_team_member) { true }

        include_examples 'event is applicable'
      end

      context 'when there is no idle or stale label' do
        let(:label_names) { [] }

        include_examples 'event is not applicable'
      end

      context 'when event is an MR update' do
        include_context 'with event', 'Triage::MergeRequestEvent' do
          let(:event_attrs) do
            {
              by_resource_author?: true,
              revision_update?: revision_update
            }
          end
        end

        let(:label_names) { [Labels::IDLE_LABEL] }

        context 'when MR update event is a revision update' do
          let(:revision_update) { true }

          include_examples 'event is applicable'
        end

        context 'when MR update event is not a revision update' do
          let(:revision_update) { false }

          include_examples 'event is not applicable'
        end
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message to remove the labels' do
      body = <<~MARKDOWN.chomp
        /unlabel ~"#{Labels::IDLE_LABEL}" ~"#{Labels::STALE_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
