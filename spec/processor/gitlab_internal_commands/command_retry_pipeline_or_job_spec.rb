# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/processor/gitlab_internal_commands/command_retry_pipeline_or_job'

RSpec.describe Triage::CommandRetryPipelineOrJob do
  include_context 'with event', 'Triage::IssueNoteEvent' do
    let(:event_attrs) do
      {
        new_comment: command_comment,
        event_actor: triager,
        from_master_broken_incidents_project?: from_master_broken_incidents_project,
        label_names: label_names,
        by_team_member?: by_team_member,
        payload: { 'object_attributes' => { 'discussion_id' => discussion_id } }
      }
    end

    let(:command_comment) { '@gitlab-bot retry_job 123' }
    let(:triager) { Triage::User.new(username: 'mr_author_username') }
    let(:from_master_broken_incidents_project) { true }
    let(:label_names) { ['master:broken'] }
    let(:by_team_member) { true }
    let(:discussion_id) { 1 }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.note']

  describe '#applicable?' do
    it_behaves_like 'applicable on contextual event'

    context 'when command is retry_pipeline' do
      let(:command_comment) { '@gitlab-bot retry_pipeline 123' }

      include_examples 'event is applicable'
    end

    context 'when pipeline label contains master-foss:broken' do
      let(:label_names) { ['master:foss-broken'] }

      include_examples 'event is applicable'
    end

    context 'when actor is not a team member' do
      let(:by_team_member) { false }

      include_examples 'event is not applicable'
    end

    context 'when resouce issue is not from master broken incident project' do
      let(:from_master_broken_incidents_project) { false }

      include_examples 'event is not applicable'
    end

    context 'when command name is not valid' do
      let(:command_comment) { '@gitlab-bot retry_invalid 234' }

      include_examples 'event is not applicable'
    end

    context 'when issue label does not indicate pipeline project' do
      let(:label_names) { ['backend'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'retry pipeline or job' do |resource_type|
      let(:args_id)                 { '123' }
      let(:web_url)                 { 'web_url' }
      let(:response_request_double) { double('request', base_uri: '', path: '') }
      let(:client_request)          { resource_type == 'pipeline' ? :retry_pipeline : :job_retry }

      before do
        event_attrs[:new_comment] = "@gitlab-bot retry_#{resource_type} #{args_id}"
        allow(Triage.api_client).to receive(client_request).and_return({ 'web_url' => web_url })
      end

      it_behaves_like 'rate limited discussion notes requests', count: 100, period: 3600

      context 'with incident labeled with master:broken' do
        let(:label_names) { ['master:broken'] }

        it "replies with retried #{resource_type} url" do
          expect_discussion_notes_request(
            event: event,
            body: "Retried #{resource_type} at #{web_url}."
          ) do
            subject.process
          end
        end
      end

      context 'with incident labeled with master:foss-broken' do
        let(:label_names) { ['master:foss-broken'] }

        it "replies with retried #{resource_type} url" do
          expect_discussion_notes_request(
            event: event,
            body: "Retried #{resource_type} at #{web_url}."
          ) do
            subject.process
          end
        end
      end

      context 'with incident not labeled with master:broken or master:foss-broken' do
        let(:label_names) { ['master:unkown-project-broken'] }

        it 'does nothing' do
          expect_no_request { subject.process }
        end
      end

      context 'when request returned error' do
        let(:error_response_double) do
          double('response',
            code: error_code,
            request: response_request_double,
            parsed_response: { message: 'error' })
        end

        before do
          allow(Triage.api_client).to receive(client_request).and_raise(error)
        end

        context 'with Gitlab::Error::NotFound error' do
          let(:error_code) { '404' }
          let(:error) { Gitlab::Error::NotFound.new(error_response_double) }

          it "replies with invalid #{resource_type} id" do
            expect_discussion_notes_request(
              event: event,
              body: "Command failed, #{args_id} is not a valid #{resource_type} id."
            ) do
              subject.process
            end
          end
        end

        context 'with Gitlab::Error::Forbidden' do
          let(:error_code) { '403' }
          let(:error) { Gitlab::Error::Forbidden.new(error_response_double) }

          it 'replies with forbidden retry response' do
            expect_discussion_notes_request(
              event: event,
              body: "Command failed, #{resource_type} #{args_id} cannot be retried at this time."
            ) do
              subject.process
            end
          end
        end

        context 'with other error types' do
          let(:error_code) { '500' }
          let(:error) { Gitlab::Error::InternalServerError.new(error_response_double) }

          it 'replies with general failure response' do
            expect_discussion_notes_request(
              event: event,
              body: 'Command failed! Try again later.'
            ) do
              subject.process
            end
          end
        end
      end
    end

    context 'when retrying pipeline' do
      it_behaves_like 'retry pipeline or job', 'pipeline'
    end

    context 'when retrying job' do
      it_behaves_like 'retry pipeline or job', 'job'
    end
  end
end
