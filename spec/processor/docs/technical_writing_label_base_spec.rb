# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/docs/technical_writing_label_base'
require_relative 'technical_writing_label_shared_examples'

RSpec.describe Triage::Docs::TechnicalWritingLabelBase do
  include_examples 'technical writing label features'

  describe '#documentation' do
    it 'raises NotImplementedError' do
      expect { subject.documentation }.to raise_error(NotImplementedError)
    end
  end
end
