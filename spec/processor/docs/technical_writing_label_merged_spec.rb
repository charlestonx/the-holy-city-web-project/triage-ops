# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/docs/technical_writing_label_merged'
require_relative 'technical_writing_label_shared_examples'

RSpec.describe Triage::Docs::TechnicalWritingLabelMerged do
  let(:action) { 'merge' }

  include_examples 'registers listeners', ['merge_request.merge']

  include_examples 'technical writing label features'

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end
end
