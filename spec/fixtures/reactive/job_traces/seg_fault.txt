[0KRunning with gitlab-runner 15.9.0~beta.212.g8ccc65e7 (8ccc65e7)[0;m
[0K  on green-2.private.runners-manager.gitlab.com/gitlab.com/gitlab-org GaSD-S1F, system ID: s_5651e5b5643b[0;m
[0K  feature flags: FF_NETWORK_PER_BUILD:true, FF_USE_FASTZIP:true, FF_USE_IMPROVED_URL_MASKING:true[0;m
section_start:1680531112:resolve_secrets
[0K[0K[36;1mResolving secrets[0;m[0;m
section_end:1680531112:resolve_secrets
[0Ksection_start:1680531112:prepare_executor
[0K[0K[36;1mPreparing the "docker+machine" executor[0;m[0;m
[0KUsing Docker executor with image registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-3.0.patched-golang-1.19-rust-1.65-node-16.14-postgresql-13:rubygems-3.4-git-2.36-lfs-2.9-chrome-109-yarn-1.22-graphicsmagick-1.3.36 ...[0;m
[0KStarting service postgres:13 ...[0;m
[0KPulling docker image postgres:13 ...[0;m
[0KUsing docker image sha256:dd421ca1f7f13d81c5c145d77d97d8d84cd0e6f1e045936ee506ce0f50ee397a for postgres:13 with digest postgres@sha256:00f455399f30cc3f2fe4185476601438b7a4959c74653665582d7c313a783d51 ...[0;m
[0KStarting service redis:6.2-alpine ...[0;m
[0KPulling docker image redis:6.2-alpine ...[0;m
[0KUsing docker image sha256:3616f0c0705d2a35d30dde109daf3cbe58ae7284121aafa6f5cfa987db98d1a8 for redis:6.2-alpine with digest redis@sha256:edddbcad5a41d58df2f142d68439922f1860ea902903d016257337c3342f30fc ...[0;m
[0KWaiting for services to be up and running (timeout 30 seconds)...[0;m
[0KAuthenticating with credentials from job payload (GitLab Registry)[0;m
[0KPulling docker image registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-3.0.patched-golang-1.19-rust-1.65-node-16.14-postgresql-13:rubygems-3.4-git-2.36-lfs-2.9-chrome-109-yarn-1.22-graphicsmagick-1.3.36 ...[0;m
[0KUsing docker image sha256:0a7a7c2f9aabd4679c452ef6b0b8799152afdbf9e25a5512e125b526f50086c8 for registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-3.0.patched-golang-1.19-rust-1.65-node-16.14-postgresql-13:rubygems-3.4-git-2.36-lfs-2.9-chrome-109-yarn-1.22-graphicsmagick-1.3.36 with digest registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-3.0.patched-golang-1.19-rust-1.65-node-16.14-postgresql-13@sha256:446301029b2bb26682cbec9a06392f5356d76d2b9b0839accae7923d9645b98c ...[0;m
section_end:1680531120:prepare_executor
[0Ksection_start:1680531120:prepare_script
[0K[0K[36;1mPreparing environment[0;m[0;m
Running on runner-gasd-s1f-project-278964-concurrent-0 via runner-gasd-s1f-private-1680515872-711ff52f...
section_end:1680531121:prepare_script
[0Ksection_start:1680531121:get_sources
[0K[0K[36;1mGetting source from Git repository[0;m[0;m
[32;1m$ eval "$CI_PRE_CLONE_SCRIPT"[0;m
[32;1mFetching changes with git depth set to 20...[0;m
Initialized empty Git repository in /builds/gitlab-org/gitlab/.git/
[32;1mCreated fresh repository.[0;m

Knapsack global time execution for tests: 22m 22s

Finished in 22 minutes 28 seconds (files took 42.69 seconds to load)
469 examples, 0 failures

Randomized with seed 31854

[TEST PROF INFO] Time spent in factories: 01:43.017 (7.63% of total time)
Failed to write to log, write log/workhorse-test.log: file already closed
./scripts/rspec_helpers.sh: line 162:   355 Segmentation fault      (core dumped) tooling/bin/parallel_rspec --rspec_args "$(rspec_args "${rspec_opts}")"
[0;33mRSpec exited with 139.
[0m[0;32mNo examples to retry, congrats!
[0msection_end:1680532674:step_script
[0Ksection_start:1680532674:upload_artifacts_on_failure
[0K[0K[36;1mUploading artifacts for failed job[0;m[0;m
[32;1mUploading artifacts...[0;m
coverage/: found 5 matching artifact files and directories[0;m
crystalball/: found 2 matching artifact files and directories[0;m
[0;33mWARNING: deprecations/: no matching files. Ensure that the artifact path is relative to the working directory (/builds/gitlab-org/gitlab)[0;m
knapsack/: found 3 matching artifact files and directories[0;m
query_recorder/: found 2 matching artifact files and directories[0;m
rspec/: found 8 matching artifact files and directories[0;m
[0;33mWARNING: tmp/capybara/: no matching files. Ensure that the artifact path is relative to the working directory (/builds/gitlab-org/gitlab)[0;m
log/*.log: found 16 matching artifact files and directories[0;m
[0;33mWARNING: Upload request redirected                [0;m  [0;33mlocation[0;m=https://gitlab.com/api/v4/jobs/4051209939/artifacts?artifact_format=zip&artifact_type=archive&expire_in=31d [0;33mnew-url[0;m=https://gitlab.com
[0;33mWARNING: Retrying...                              [0;m  [0;33mcontext[0;m=artifacts-uploader [0;33merror[0;m=request redirected
Uploading artifacts as "archive" to coordinator... 201 Created[0;m  id[0;m=4051209939 responseStatus[0;m=201 Created token[0;m=64_2oHvt
[32;1mUploading artifacts...[0;m
rspec/junit_rspec.xml: found 1 matching artifact files and directories[0;m
[0;33mWARNING: Upload request redirected                [0;m  [0;33mlocation[0;m=https://gitlab.com/api/v4/jobs/4051209939/artifacts?artifact_format=gzip&artifact_type=junit&expire_in=31d [0;33mnew-url[0;m=https://gitlab.com
[0;33mWARNING: Retrying...                              [0;m  [0;33mcontext[0;m=artifacts-uploader [0;33merror[0;m=request redirected
Uploading artifacts as "junit" to coordinator... 201 Created[0;m  id[0;m=4051209939 responseStatus[0;m=201 Created token[0;m=64_2oHvt
section_end:1680532681:upload_artifacts_on_failure
[0Ksection_start:1680532681:cleanup_file_variables
[0K[0K[36;1mCleaning up project directory and file based variables[0;m[0;m
section_end:1680532682:cleanup_file_variables
[0K[31;1mERROR: Job failed: exit code 139
[0;m
