# frozen_string_literal: true
#

RSpec.shared_examples 'event is not applicable' do
  it 'is not applicable' do
    expect(subject).not_to be_applicable
  end

  it 'does not process the event' do
    expect(subject).not_to receive(:process)

    subject.triage
  end
end

RSpec.shared_examples 'event is applicable' do
  it 'is applicable' do
    expect(subject).to be_applicable
  end
end

RSpec.shared_examples 'applicable on contextual event' do
  context 'when all conditions meet' do
    include_examples 'event is applicable'
  end
end
