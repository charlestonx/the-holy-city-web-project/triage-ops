# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/group_triage_helper'

class DummyGroupTriageHelper
  include GroupTriageHelperContext

  def titled_heatmap(title)
    "Stub heatmap with title: #{title}"
  end
end

RSpec.shared_context 'with report metadata' do
  let(:assignees) { ['@user1'] }
  let(:labels)    { %w[first-label second-label] }
  let(:mentions)  { ['@user2 @user3'] }
end

RSpec.shared_examples 'report summaries' do
  include_context 'with report metadata'

  describe 'labels' do
    it 'sets some default labels' do
      expect(subject).to match('/label ~"triage report" ~"type::ignore"')
    end

    it 'sets the provided labels' do
      expect(subject).to match('/label ~"first-label" ~"second-label"')
    end
  end

  it 'sets a due date' do
    expect(subject).to match("/due #{described_class::REPORT_DUE}")
  end

  context 'when assignees are present' do
    let(:assignees) { ['@user1'] }

    context 'when no mentions are present' do
      let(:mentions) { [] }

      it 'only includes assignees in the greeting' do
        expect(subject).to match("Hi `@user1`,\n")
      end

      it 'does not add a /cc command' do
        expect(subject).not_to match("/cc")
      end
    end

    context 'when mentions are present' do
      it 'includes assignees (backticked)' do
        expect(subject).to match("Hi `@user1`,\n")
      end

      it 'adds a /cc command with the mentions' do
        expect(subject).to match("/cc @user2 @user3\n")
      end
    end
  end
end

RSpec.describe DummyGroupTriageHelper do
  let(:instance) { described_class.new }

  describe '#merge_requests_needing_attention_report' do
    subject { instance.merge_requests_needing_attention_report(assignees: assignees, labels: labels, mentions: mentions) }

    it_behaves_like 'report summaries'
  end

  describe '#short_team_summary' do
    subject { instance.short_team_summary(title: title, assignees: assignees, labels: labels, mentions: mentions) }

    let(:title) { 'A very specific title' }

    it_behaves_like 'report summaries'

    context 'when a title is provided' do
      include_context 'with report metadata'

      it 'shows the title in the description' do
        expect(subject).to match(title)
      end
    end
  end

  describe "#report_summary" do
    subject { instance.report_summary(assignees: assignees, labels: labels, mentions: mentions) }

    it_behaves_like 'report summaries'
  end

  describe "#build_command" do
    let(:strings) { %w[user1 user2 user3] }

    context 'with no flags' do
      subject { instance.build_command(strings) }

      it 'returns the correct string' do
        expect(subject).to eq("user1 user2 user3")
      end
    end

    context 'with a prefix' do
      subject { instance.build_command(strings, prefix: 'hey-') }

      it 'returns the correct string' do
        expect(subject).to eq("hey-user1 hey-user2 hey-user3")
      end
    end

    context 'with the quote flag' do
      subject { instance.build_command(strings, quote: true) }

      it 'returns the correct string' do
        expect(subject).to eq("\"user1\" \"user2\" \"user3\"")
      end
    end

    context 'with the backticks flag' do
      subject { instance.build_command(strings, backticks: true) }

      it 'returns the correct string' do
        expect(subject).to eq("`user1` `user2` `user3`")
      end
    end

    context 'with several flags' do
      subject { instance.build_command(strings, prefix: 'a-', quote: true, backticks: true) }

      it 'returns the correct string' do
        expect(subject).to eq("`a-\"user1\"` `a-\"user2\"` `a-\"user3\"`")
      end
    end
  end

  describe "#build_mentions" do
    let(:assignees) { %w[user1 user2 user3] }

    subject { instance.build_mentions(assignees) }

    it 'returns a string of assignees' do
      expect(subject).to eq("@user1 @user2 @user3")
    end
  end

  describe "#build_backticked_mentions" do
    let(:assignees) { %w[user1 user2 user3] }

    subject { instance.build_backticked_mentions(assignees) }

    it 'returns a string of backticked assignees' do
      expect(subject).to eq("`@user1` `@user2` `@user3`")
    end
  end

  describe "#missed_slo_heatmap" do
    it 'returns a heatmap titled as Heatmap for ~SLO::Missed bug' do
      expect(instance.missed_slo_heatmap).to eq(
        "Stub heatmap with title: ### Heatmap for ~SLO::Missed bugs"
      )
    end
  end

  describe "#merge_group" do
    include GroupDefinition

    let(:assignees) { nil }

    subject { instance.merge_group(group_contributor_success, assignees: assignees) }

    context 'when provided assignees is nil' do
      it 'keeps the same assignees' do
        expect(subject[:assignees]).to match_array(group_contributor_success[:assignees])
      end
    end

    context 'when provided assignees is not nil' do
      let(:assignees) { ["@taucher2003", "@zillemarco"] }

      it 'merges the original assignees with the provided ones' do
        expect(subject[:assignees]).to match_array(
          group_contributor_success[:assignees] + ["@taucher2003", "@zillemarco"])
      end
    end
  end

  describe '#default_greetings' do
    subject { instance.default_greetings(assignees) }

    context 'when the assignees are nil' do
      let(:assignees) { nil }

      it 'returns an empty string' do
        expect(subject).to be_empty
      end
    end

    context 'when the assignees are empty' do
      let(:assignees) { [] }

      it 'returns an empty string' do
        expect(subject).to be_empty
      end
    end

    context 'when there are assignees' do
      let(:assignees) { %w[@user1 @user2] }

      it 'returns a greeting for the assignees (backticked)' do
        expect(subject).to eq('Hi `@user1` `@user2`,')
      end
    end
  end

  describe '#in_cc' do
    subject { instance.in_cc(mentions) }

    context 'when the mentions are nil' do
      let(:mentions) { nil }

      it 'returns an empty string' do
        expect(subject).to be_empty
      end
    end

    context 'when the mentions are empty' do
      let(:mentions) { [] }

      it 'returns an empty string' do
        expect(subject).to be_empty
      end
    end

    context 'when there are mentions' do
      let(:mentions) { %w[@user3 @user4] }

      it 'returns a /cc command for the mentions' do
        expect(subject).to eq('/cc @user3 @user4')
      end
    end
  end
end
