# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/stage_definition'

RSpec.describe StageDefinition do
  described_class::STAGE_DATA.each_key do |name|
    describe "#stage_#{name}" do
      it 'returns assignees, labels and groups as arrays, optionally mentions too' do
        subject = described_class.public_send("stage_#{name}")

        expect(subject).to include(
          assignees: a_kind_of(Array),
          labels: a_kind_of(Array),
          groups: a_kind_of(Hash)
        )

        expect(subject[:mentions]).to be_a_kind_of(Array).or(be_nil)
      end

      it 'returns assignees only from group pms if specified' do
        subject = described_class.public_send("stage_#{name}", ['pm'])

        assignees =
          described_class::STAGE_DATA.dig(name, 'groups').flat_map do |group|
            described_class::GROUP_DATA.dig(group, 'pm')
          end.compact.uniq

        expect(subject).to include(assignees: assignees)
      end

      it 'returns pms and backend_engineering_managers from constituent groups as assignees when specified' do
        subject = described_class.public_send("stage_#{name}", %w[pm backend_engineering_manager])

        groups = described_class::STAGE_DATA.dig(name, 'groups')
        assignees = groups.flat_map do |group|
          (
            (described_class::GROUP_DATA.dig(group, 'pm') || []) +
            (described_class::GROUP_DATA.dig(group, 'backend_engineering_manager') || [])
          )
        end.uniq

        expect(subject[:assignees]).to contain_exactly(*assignees)
      end

      it 'returns mentions with extra mentions if specified' do
        subject = described_class.public_send(
          "stage_#{name}", %w[pm extra_mentions])

        groups = described_class::STAGE_DATA.dig(name, 'groups')

        assignees = groups.flat_map do |group|
          described_class::GROUP_DATA.dig(group, 'pm')
        end.compact.uniq

        extra_mentions = groups.flat_map do |group|
          described_class::GROUP_DATA.dig(group, 'extra_mentions')
        end.compact.uniq

        mentions = extra_mentions if extra_mentions.any?

        if mentions
          expect(subject).to include(assignees: assignees, mentions: mentions.uniq)
        else
          expect(subject).to include(assignees: assignees)
        end
      end

      it 'returns defined labels or guessed stage labels' do
        subject = described_class.public_send("stage_#{name}")

        labels = described_class::STAGE_DATA.dig(name, 'labels') ||
          ["devops::#{name.tr('_', ' ')}"]

        expect(subject).to include(labels: labels)
      end

      it 'returns group information' do
        subject = described_class.public_send("stage_#{name}")

        group_names = described_class::STAGE_DATA.dig(name, 'groups')
        groups = group_names.each_with_object({}) do |n, acc|
          acc[n] = described_class::GROUP_DATA[n]
        end.compact

        expect(subject).to include(groups: groups)
      end
    end
  end
end
